package example.Library.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.OffsetDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public final ErrorResponseDto handleResourceNotFound(ResourceNotFoundException ex, WebRequest request) {
        return ErrorResponseDto.builder()
                .message("Not Found")
                .code(ex.getErrorCode())
                .detail(ex.getMessage())
                .status(HttpStatus.NOT_FOUND.value())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
    }

    @ExceptionHandler(CustomerDataNotCompleteException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ErrorResponseDto handleCustomerDataNotComplete(CustomerDataNotCompleteException ex, WebRequest request) {
        return ErrorResponseDto.builder()
                .message("Bad Request")
                .code(ex.getErrorCode())
                .detail(ex.getMessage())
                .status(HttpStatus.BAD_REQUEST.value())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
    }

    @ExceptionHandler(UsernameExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ErrorResponseDto handleUsernameExists(UsernameExistsException ex, WebRequest request) {
        return ErrorResponseDto.builder()
                .message("Bad Request")
                .code(ex.getErrorCode())
                .detail(ex.getMessage())
                .status(HttpStatus.BAD_REQUEST.value())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
    }

    @ExceptionHandler(CustomerExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ErrorResponseDto handleCustomerExists(CustomerExistsException ex, WebRequest request) {
        return ErrorResponseDto.builder()
                .message("Bad Request")
                .code(ex.getErrorCode())
                .detail(ex.getMessage())
                .status(HttpStatus.BAD_REQUEST.value())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
    }

    @ExceptionHandler(StockEmptyException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public final ErrorResponseDto handleStockEmpty(StockEmptyException ex, WebRequest request) {
        return ErrorResponseDto.builder()
                .message("Not Found")
                .code(ex.getErrorCode())
                .detail(ex.getMessage())
                .status(HttpStatus.NOT_FOUND.value())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
    }
}
