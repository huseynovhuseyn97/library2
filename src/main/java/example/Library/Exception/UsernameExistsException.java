package example.Library.Exception;

import lombok.Getter;

@Getter
public class UsernameExistsException extends RuntimeException{

    private final ErrorCode errorCode = ErrorCode.USERNAME_ALREADY_EXISTS;
    private final String resourceValue;

    public UsernameExistsException(String resourceValue) {
        super(String.format("Username %s already exists", resourceValue));
        this.resourceValue = resourceValue;
    }

}
