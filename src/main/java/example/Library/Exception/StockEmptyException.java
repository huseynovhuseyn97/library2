package example.Library.Exception;

import lombok.Getter;

@Getter
public class StockEmptyException extends RuntimeException {
    private final ErrorCode errorCode;
    public StockEmptyException(ErrorCode errorCode) {
        super("Stock is empty");
        this.errorCode = errorCode;
    }
}
