package example.Library.Exception;

import lombok.Getter;

@Getter
public class CustomerExistsException extends RuntimeException{

    private final ErrorCode errorCode;
    private final String resourceValue;
    public CustomerExistsException(ErrorCode errorCode, String resourceValue) {
        super(String.format("Customer %s already exists", resourceValue));
        this.errorCode = errorCode;
        this.resourceValue = resourceValue;
    }
}
