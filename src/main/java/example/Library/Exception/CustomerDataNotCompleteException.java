package example.Library.Exception;

import example.Library.Exception.ErrorCode;
import lombok.Getter;

@Getter
public class CustomerDataNotCompleteException extends RuntimeException {
    private final ErrorCode errorCode = ErrorCode.CUSTOMER_DATA_NOT_COMPLETE;

    public CustomerDataNotCompleteException() {
        super("Customer data not complete");
    }
}
