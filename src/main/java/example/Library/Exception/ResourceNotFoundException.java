package example.Library.Exception;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends RuntimeException{
    private final String resourceName;
    private final String resourceField;
    private final Object fieldValue;
    private final ErrorCode errorCode;

    public ResourceNotFoundException(String resourceName, String resourceField, Object fieldValue, ErrorCode errorCode) {
        super(String.format("%s not found with %s : '%s'", resourceName, resourceField, fieldValue));
        this.resourceName = resourceName;
        this.resourceField = resourceField;
        this.fieldValue = fieldValue;
        this.errorCode = errorCode;
    }

}
