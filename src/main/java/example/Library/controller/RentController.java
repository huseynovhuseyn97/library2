package example.Library.controller;

import example.Library.DTO.requestDto.RequestRentDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseAuthorDto;
import example.Library.DTO.responseDto.ResponseRentDto;
import example.Library.service.RentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rent")
@RequiredArgsConstructor
public class RentController {

    private final RentService rentService;


    @GetMapping("/all/{pageSize}/{pageNumber}")
    public PageResponse<ResponseRentDto> getAll(@PathVariable("pageSize") int pageSize,
                                                @PathVariable("pageNumber") int pageNumber) {
        return rentService.findAll(pageNumber, pageSize);
    }

    @GetMapping("/get/{id}")
    public ResponseRentDto findById(@PathVariable("id") Long id) {
        return rentService.findById(id);
    }

    @PostMapping("/create")
    public void creatRent (@RequestBody RequestRentDto requestRentDto){
    rentService.createRent(requestRentDto);
    }
}


