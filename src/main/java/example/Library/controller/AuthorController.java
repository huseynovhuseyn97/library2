package example.Library.controller;

import example.Library.DTO.requestDto.RequestAuthorDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseAuthorDto;
import example.Library.service.AuthorService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {

    public final AuthorService authorService;

    @GetMapping("/all/{pageSize}/{pageNumber}")
    public PageResponse<ResponseAuthorDto> getAll(@PathVariable("pageSize") int pageSize,
                                                  @PathVariable("pageNumber") int pageNumber) {
        return authorService.findAll(pageNumber, pageSize);
    }

    @GetMapping("/get/{id}")
    public ResponseAuthorDto findById(@PathVariable("id") Long id) {
        return authorService.findById(id);
    }

    @PostMapping("/create")
    public void create(@RequestBody @Valid RequestAuthorDto requestAuthorDto) {

        authorService.createAuthor(requestAuthorDto);
    }

    @PutMapping("/update/{id}")
    public void update(@PathVariable("id") Long id, @RequestBody RequestAuthorDto requestAuthorDto) {
        authorService.update(id, requestAuthorDto);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        authorService.deleteAuthor(id);
    }
}
