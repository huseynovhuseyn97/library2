package example.Library.controller;

import example.Library.DTO.requestDto.RequestTypeDto;
import example.Library.DTO.responseDto.ResponseTypeDto;
import example.Library.service.TypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/type")
public class TypeController {

    public final TypeService typeService;



    @GetMapping("/all")
    public List<ResponseTypeDto> getAll() {
        return typeService.findAll();
    }

    @GetMapping("get/{id}")
    public ResponseTypeDto findById(@PathVariable("id") Long id) {
        return typeService.findById(id);
    }

    @PostMapping("/create")
    public void create(@RequestBody RequestTypeDto typeDto) {
        typeService.createType(typeDto);
    }

    @PutMapping("/update/{id}")
    public void update(@PathVariable("id") Long id, @RequestBody RequestTypeDto typeDto) {
        typeService.update(id, typeDto);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        typeService.deleteType(id);
    }
}