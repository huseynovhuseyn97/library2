package example.Library.controller;

import example.Library.DTO.requestDto.RequestBooksDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseBooksDto;
import example.Library.service.BooksService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BooksController {

    private final BooksService booksService;


    @GetMapping("/all/{pageSize}/{pageNumber}")
    public PageResponse<ResponseBooksDto> getAll(@PathVariable("pageSize") int pageSize,
                                                 @PathVariable("pageNumber") int pageNumber) {
        return booksService.findAll(pageNumber, pageSize);
    }

    @GetMapping("/get/{id}")
    public ResponseBooksDto findBook(@PathVariable("id") Long id) {
        return booksService.findBook(id);
    }

    @PostMapping("/create")
    public void creatBook(@RequestBody RequestBooksDto requestBooksDto) {
        booksService.createBook(requestBooksDto);
    }

    @PutMapping("/update/{id}")
    public void updateBook(@PathVariable("id") Long id, @RequestBody RequestBooksDto requestBooksDto) {
        booksService.updateBook(id, requestBooksDto);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteBook(@PathVariable("id") Long id) {
        booksService.deleteBook(id);
    }
}