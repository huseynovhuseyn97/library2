package example.Library.controller;

import example.Library.DTO.requestDto.RequestCustomerDto;
import example.Library.DTO.requestDto.RequestSysUserDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseBooksDto;
import example.Library.DTO.responseDto.ResponseCustomerDto;
import example.Library.domain.SysUser;
import example.Library.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;


    @GetMapping("/all/{pageSize}/{pageNumber}")
    public PageResponse<ResponseCustomerDto> getAll(@PathVariable("pageSize") int pageSize,
                                                    @PathVariable("pageNumber") int pageNumber) {
        return customerService.findAll(pageNumber, pageSize);
    }

    @GetMapping("/get/{id}")
    public ResponseCustomerDto findCustomer(@PathVariable("id") Long id) {
        return customerService.findCustomer(id);
    }
    @PostMapping("/create")
    public void createCustomer(@RequestBody RequestCustomerDto customerDto){
        customerService.createCustomer(customerDto);
    }
    @GetMapping("/check/{checkUserId}")
    public void checkCustomer(@PathVariable ("checkUserId") Long checkUserId){
        customerService.checkCustomer(checkUserId);

    }
    @PutMapping("/update")
    public void updateCustomerDetails (@RequestBody RequestCustomerDto requestCustomerDto){
        customerService.updateCustomerDetails(requestCustomerDto);
    }
    }
