package example.Library.controller;

import example.Library.DTO.requestDto.RequestTagsDto;
import example.Library.DTO.responseDto.ResponseTagsDto;
import example.Library.service.TagsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tags")
@RequiredArgsConstructor
public class TagsController {

    private final TagsService tagsService;


    @GetMapping("/all")
    public List<ResponseTagsDto> getAll() {
        return tagsService.findAll();
    }

    @GetMapping("/get/{id}")
    public ResponseTagsDto findById(@PathVariable("id") Long id) {
        return tagsService.findById(id);
    }

    @PostMapping("/create")
    public void create(@RequestBody RequestTagsDto tagsDto) {
        tagsService.createTag(tagsDto);
    }

    @PutMapping("/update/{id}")
    public void update(@PathVariable("id") Long id, @RequestBody RequestTagsDto tagsDto) {
        tagsService.update(id, tagsDto);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        tagsService.deleteTag(id);
    }
}

