package example.Library.controller;

import example.Library.DTO.requestDto.RequestLoginDto;
import example.Library.DTO.requestDto.RequestSysUserDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseSysUserDto;
import example.Library.DTO.responseDto.ResponseTokenDto;
import example.Library.domain.Role;
import example.Library.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class SysUserController {

    private final SysUserService sysUserService;


    @GetMapping("/all/{pageSize}/{pageNumber}")
    public PageResponse<ResponseSysUserDto> findAll(@PathVariable("pageSize") int pageSize,
                                                    @PathVariable("pageNumber") int pageNumber) {
        return sysUserService.findAll(pageNumber, pageSize);
    }

    @GetMapping("/get/{id}")
    public ResponseSysUserDto findUser(@PathVariable("id") Long id) {
        return sysUserService.findUser(id);
    }

    @PostMapping("/login")
    public ResponseTokenDto loginUser(@RequestBody RequestLoginDto loginDto) {
        return sysUserService.loginUser(loginDto);
    }

    @PostMapping("/create")
    public void createUser(@RequestBody RequestSysUserDto sysUserDto) {
        sysUserService.createUser(sysUserDto);
    }

    @PutMapping("/addRole/{id}/role")
    public void updateRole(@PathVariable("id") Long id, @RequestParam(value = "role") Role newRole) {
        sysUserService.addNewRole(id, newRole);
    }
}


