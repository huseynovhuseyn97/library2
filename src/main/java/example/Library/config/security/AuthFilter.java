package example.Library.config.security;

import example.Library.domain.SysUser;
import example.Library.service.SysUserService;
import example.Library.service.impl.SysUserServiceImpl;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor

public class AuthFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Optional<Authentication> authenticationOptional = Optional.empty();
        try {
            authenticationOptional = authenticationOptional.or(() -> jwtService.getAuthentication(request));
            System.out.println("authenication" + authenticationOptional.isPresent());
            authenticationOptional.ifPresent(authentication -> SecurityContextHolder.getContext().setAuthentication(authentication));
            System.out.println("authenication 2" + authenticationOptional.isPresent());

            filterChain.doFilter(request, response);
        } catch (ExpiredJwtException e) {
            logger.error("Error in AuthFilter",e);
        }


    }
}
