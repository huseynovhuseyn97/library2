package example.Library.config.security;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import example.Library.DTO.responseDto.ResponseTokenDto;
import example.Library.domain.Authority;
import example.Library.domain.SysUser;
import example.Library.service.impl.SysUserServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class JwtService {

    SysUserServiceImpl sysUserService;

    private Key key;
    @Value("${security.jwtProperties.secret}")
    private String secretKey;

    @PostConstruct
    public void init() {
        byte[] bytes = Decoders.BASE64.decode(secretKey);
        key = Keys.hmacShaKeyFor(bytes);
    }

    public ResponseTokenDto issueToken(UserDetails user) {
        String token = Jwts.builder().setSubject(user
                        .getUsername())
//                .claim("UserID",user.getId())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now()
                        .plus(Duration.ofMinutes(15))))
                .signWith(key, SignatureAlgorithm.HS256)
                .claim("role", user.getAuthorities())
                .compact();
        return new ResponseTokenDto(token);
    }


    public Optional<? extends Authentication> getAuthentication(HttpServletRequest request) {

        String authorization = request.getHeader("Authorization");
        System.out.println(authorization + "    aaaaa");
        if (authorization == null) {
            System.out.println("sakaaaam");
            return Optional.empty();
        }
        String token = authorization.substring(7);
        System.out.println("222" + token);
        Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);

        Claims body = claimsJws.getBody();
        if (body.getExpiration().before(new Date())) {
            throw new IllegalArgumentException("Token Expired");
        }
        return Optional.of(getAuthenticationBearer(body));
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        System.out.println(claims.toString());
        List<Map<String, String>> roles =  claims.get("role", List.class);
        List<SimpleGrantedAuthority> authorityList = roles
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.get("authority")))
                .collect(Collectors.toList());
        System.out.println("getBearer");
       User details = new User(claims.getSubject(), "", authorityList);

        return new UsernamePasswordAuthenticationToken(details,null,authorityList);
    }
}
