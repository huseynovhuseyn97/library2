package example.Library.service;

import example.Library.DTO.requestDto.RequestAuthorDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseAuthorDto;

public interface AuthorService {

    void createAuthor(RequestAuthorDto requestAuthorDto);

    PageResponse<ResponseAuthorDto> findAll(int pageNumber, int pageSize);

    ResponseAuthorDto findById(Long id);

    void update(Long id, RequestAuthorDto requestAuthorDto);

    void deleteAuthor(Long id);
}
