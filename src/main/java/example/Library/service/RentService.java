package example.Library.service;

import example.Library.DTO.requestDto.RequestRentDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseRentDto;

public interface RentService {
   void createRent(RequestRentDto requestRentDto);

   PageResponse<ResponseRentDto> findAll(int pageNumber, int pageSize);

   ResponseRentDto findById(Long id);
}
