package example.Library.service;

import example.Library.DTO.requestDto.RequestLoginDto;
import example.Library.DTO.requestDto.RequestSysUserDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseSysUserDto;
import example.Library.DTO.responseDto.ResponseTokenDto;
import example.Library.domain.Role;
import example.Library.domain.SysUser;

public interface SysUserService {
    void createUser(RequestSysUserDto requestSysUserDto);

    ResponseTokenDto loginUser(RequestLoginDto loginDto);

    void addNewRole(Long id, Role newRole);

    SysUser getUser();

    PageResponse<ResponseSysUserDto> findAll(int pageNumber, int pageSize);

    ResponseSysUserDto findUser(Long id);
}
