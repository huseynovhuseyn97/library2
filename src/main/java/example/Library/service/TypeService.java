package example.Library.service;

import example.Library.DTO.requestDto.RequestTypeDto;
import example.Library.DTO.responseDto.ResponseTypeDto;

import java.util.List;

public interface TypeService {
    void createType(RequestTypeDto typeDto);

    List<ResponseTypeDto> findAll();

    ResponseTypeDto findById(Long id);

    void update(Long id, RequestTypeDto typeDto);

    void deleteType(Long id);
}
