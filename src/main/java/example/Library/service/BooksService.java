package example.Library.service;

import example.Library.DTO.requestDto.RequestBooksDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseAuthorDto;
import example.Library.DTO.responseDto.ResponseBooksDto;

import java.util.List;

public interface BooksService {

    void createBook(RequestBooksDto requestBooksDto);


    PageResponse<ResponseBooksDto> findAll(int pageNumber, int pageSize);

    ResponseBooksDto findBook(Long id);

    void updateBook(Long id, RequestBooksDto requestBooksDto);

    void deleteBook(Long id);

    void changeStock(Long id);
}
