package example.Library.service.impl;

import example.Library.DTO.requestDto.RequestAuthorDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseAuthorDto;
import example.Library.Exception.ErrorCode;
import example.Library.Exception.ResourceNotFoundException;
import example.Library.domain.Author;
import example.Library.mapper.AuthorMapper;
import example.Library.repository.AuthorRepository;
import example.Library.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class AuthorServiceImpl implements AuthorService {

    public final AuthorRepository authorRepository;
    public final AuthorMapper authorMapper;

    @Override
    public void createAuthor(RequestAuthorDto requestAuthorDto) {
        Author author = authorMapper.dtoToAuthor(requestAuthorDto);
        authorRepository.save(author);
    }

    @Override
    public PageResponse<ResponseAuthorDto> findAll(int pageNumber, int pageSize) {

        var authorPage = authorRepository.findAll(PageRequest.of(pageNumber, pageSize));
        return new PageResponse<ResponseAuthorDto>()
                .setPageNumber(pageNumber)
                .setPageSize(pageSize)
                .setTotalPages(authorPage.getTotalPages())
                .setTotalElements(authorPage.getTotalElements())
                .setData(authorMapper.listToListDto(authorPage.toList()));
    }

    @Override
    public ResponseAuthorDto findById(Long id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("AUTHOR", "ID", id, ErrorCode.AUTHOR_ID_NOT_FOUND));
        return authorMapper.authorToDto(author);
    }

    @Override
    public void update(Long id, RequestAuthorDto requestAuthorDto) {
        Author author1 = authorMapper.dtoToAuthor(requestAuthorDto);
        author1.setId(id);
        authorRepository.save(author1);
    }

    @Override
    public void deleteAuthor(Long id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("AUTHOR", "ID", id, ErrorCode.AUTHOR_ID_NOT_FOUND));
        authorRepository.delete(author);
    }
}
