package example.Library.service.impl;

import example.Library.DTO.requestDto.RequestLoginDto;
import example.Library.DTO.requestDto.RequestSysUserDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseSysUserDto;
import example.Library.DTO.responseDto.ResponseTokenDto;
import example.Library.Exception.UsernameExistsException;
import example.Library.config.security.JwtService;
import example.Library.domain.Authority;
import example.Library.domain.Role;
import example.Library.domain.SysUser;
import example.Library.mapper.SysUserMapper;
import example.Library.repository.AuhtorityRepository;
import example.Library.repository.SysUserRepository;
import example.Library.service.SysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class SysUserServiceImpl implements SysUserService, UserDetailsService {

    private final SysUserRepository sysUserRepository;
    private final AuhtorityRepository auhtorityRepository;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;
    private final SysUserMapper sysUserMapper;


    @Override
    public PageResponse<ResponseSysUserDto> findAll(int pageNumber, int pageSize) {
        var userPage = sysUserRepository.findAll(PageRequest.of(pageNumber, pageSize));
        var pageResponse = new PageResponse<ResponseSysUserDto>();
        pageResponse = pageResponse
                .setPageNumber(pageNumber)
                .setPageSize(pageSize)
                .setTotalPages(userPage.getTotalPages())
                .setTotalElements(userPage.getTotalElements())
                .setData(sysUserMapper.listToListDto(userPage.toList()));
        return pageResponse;

    }

    @Override
    public ResponseSysUserDto findUser(Long id) {
        SysUser sysUser = sysUserRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User not found with id: " + id));
        return sysUserMapper.userToDto(sysUser);
    }

    @Override
    public void createUser(RequestSysUserDto requestSysUserDto) {
        Optional<SysUser> sysUser = sysUserRepository.findByUsername(requestSysUserDto.getUsername());
        sysUser.ifPresent(user -> {
            throw new UsernameExistsException(requestSysUserDto.getUsername());
        });
        Authority userAuthority = auhtorityRepository.findByRole(Role.CUSTOMER);
        SysUser build = SysUser.builder()
                .username(requestSysUserDto.getUsername())
                .password(encoder.encode(requestSysUserDto.getPassword()))
                .authorities(Collections.singletonList(userAuthority))
                .build();
        sysUserRepository.save(build);
    }


    @Override
    public ResponseTokenDto loginUser(RequestLoginDto loginDto) {
        UserDetails userModel = loadUserByUsername(loginDto.getUsername());
        if (encoder.matches(loginDto.getPassword(), userModel.getPassword())) {
            return jwtService.issueToken(userModel);
        } else {
            throw new RuntimeException();
        }
    }

    @Override
    public void addNewRole(Long id, Role newRole) {
        Optional<SysUser> optionalUser = sysUserRepository.findById(id);
        optionalUser.ifPresentOrElse(user -> {
            Authority newAuthority = auhtorityRepository.findByRole(newRole);
            user.getAuthorities().removeIf(authority -> authority.getRole().equals(newRole));
            user.getAuthorities().add(newAuthority);
            sysUserRepository.save(user);
        }, () -> {
            throw new UsernameNotFoundException("User not found with id: " + id);
        });
    }

    @Override
    public SysUser getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication.getPrincipal() instanceof UserDetails userDetails) {
            String username = userDetails.getUsername();
            Optional<SysUser> sysUser = sysUserRepository.findByUsername(username);

            return sysUser.orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        } else {
            throw new RuntimeException("Not found User");
        }

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("load user");
        SysUser user = sysUserRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        List<GrantedAuthority> authorities = user.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getRole().toString()))
                .collect(Collectors.toList());

        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .authorities(authorities)
                .build();
    }
}

