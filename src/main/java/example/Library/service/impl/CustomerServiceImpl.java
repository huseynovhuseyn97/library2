package example.Library.service.impl;

import example.Library.DTO.requestDto.RequestCustomerDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseCustomerDto;
import example.Library.Exception.CustomerDataNotCompleteException;
import example.Library.Exception.CustomerExistsException;
import example.Library.Exception.ErrorCode;
import example.Library.Exception.ResourceNotFoundException;
import example.Library.domain.Customer;
import example.Library.domain.SysUser;
import example.Library.mapper.CustomerMapper;
import example.Library.repository.CustomerRepository;
import example.Library.repository.SysUserRepository;
import example.Library.service.CustomerService;
import example.Library.service.SysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    private final SysUserService sysUserService;
    private final CustomerMapper customerMapper;
    private final SysUserRepository sysUserRepository;

    @Override
    public Customer checkCustomer(Long checkUserId) {
        SysUser sysUser = sysUserRepository.findById(checkUserId).orElseThrow(() -> new ResourceNotFoundException("Customer", "ID", checkUserId, ErrorCode.CUSTOMER_ID_NOT_FOUND));
        if (customerRepository.findBySysUser(sysUser).isEmpty()) {
            throw new CustomerDataNotCompleteException();
        } else {
          return customerRepository.findBySysUser(sysUser).get();
        }
    }


    @Override
    public void createCustomer(RequestCustomerDto requestCustomerDto) {
        SysUser custUser = sysUserService.getUser();
        if (customerRepository.findBySysUser(custUser).isEmpty()) {
          Customer customer = customerMapper.dtoToCustomer(requestCustomerDto);
          customer.setSysUser(custUser);
            customerRepository.save(customer);
        } else {
            throw new CustomerExistsException(ErrorCode.CUSTOMER_ALREADY_EXISTS, custUser.getUsername());
        }
    }

    @Override
    public void updateCustomerDetails(RequestCustomerDto requestCustomerDto) {
        SysUser custUser = sysUserService.getUser();

        Customer customer = customerRepository.findBySysUser(custUser)
                .orElseThrow(() -> new ResourceNotFoundException("Customer", "ID", custUser.getId(), ErrorCode.CUSTOMER_ID_NOT_FOUND));
        Customer customerModel = customerMapper.dtoToCustomer(requestCustomerDto);
        customerModel.setId(customer.getId());
        customerModel.setSysUser(custUser);
        customerRepository.save(customerModel);
    }

    @Override
    public PageResponse<ResponseCustomerDto> findAll(int pageNumber, int pageSize) {

        var customerPage = customerRepository.findAll(PageRequest.of(pageNumber, pageSize));
        var pageResponse = new PageResponse<ResponseCustomerDto>();
        pageResponse = pageResponse
                .setPageNumber(pageNumber)
                .setPageSize(pageSize)
                .setTotalPages(customerPage.getTotalPages())
                .setTotalElements(customerPage.getTotalElements())
                .setData(customerMapper.listToListDto(customerPage.toList()));
        return pageResponse;
    }

    @Override
    public ResponseCustomerDto findCustomer(Long id) {

        return customerMapper.customerToDto(customerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Customer", "ID", id, ErrorCode.CUSTOMER_ID_NOT_FOUND)));
    }

}


