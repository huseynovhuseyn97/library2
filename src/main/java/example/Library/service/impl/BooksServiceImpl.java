package example.Library.service.impl;

import example.Library.DTO.requestDto.RequestBooksDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseBooksDto;
import example.Library.Exception.ErrorCode;
import example.Library.Exception.ResourceNotFoundException;
import example.Library.Exception.StockEmptyException;
import example.Library.domain.Author;
import example.Library.domain.Books;
import example.Library.domain.Tags;
import example.Library.domain.Type;
import example.Library.mapper.BookMapper;
import example.Library.repository.BooksRepository;
import example.Library.service.BooksService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

import static example.Library.Exception.ErrorCode.STOCK_UNAVAILABLE;

@RequiredArgsConstructor
@Service
@Transactional
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;
    private final BookMapper bookMapper;

    @Override
    public void createBook(RequestBooksDto requestBooksDto) {
        Author author = Author.of(requestBooksDto.getAuthorId());
        Type type = Type.of(requestBooksDto.getTypeId());
        Set<Tags> tagsSet = requestBooksDto.getTagsIds().stream().map(Tags::of).collect(Collectors.toSet());

        Books books = Books.builder()
                .stock(requestBooksDto.getStock())
                .name(requestBooksDto.getName())
                .author(author)
                .tagsSet(tagsSet)
                .enable(requestBooksDto.getEnable())
                .type(type)
                .build();
        booksRepository.save(books);
    }

    @Override
    @Transactional
    public PageResponse<ResponseBooksDto> findAll(int pageNumber, int pageSize) {
        var bookPage = booksRepository.findAll(PageRequest.of(pageNumber, pageSize));

        var pageResponse = new PageResponse<ResponseBooksDto>();
        pageResponse = pageResponse
                .setPageNumber(pageNumber)
                .setPageSize(pageSize)
                .setTotalPages(bookPage.getTotalPages())
                .setTotalElements(bookPage.getTotalElements())
                .setData(bookMapper.listToListDto(bookPage.toList()));
        return pageResponse;
    }

    @Override
    public ResponseBooksDto findBook(Long id) {
        Books books = booksRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("BOOK", "ID", id, ErrorCode.BOOK_ID_NOT_FOUND));
        return bookMapper.bookToDto(books);
    }

    @Override
    public void updateBook(Long id, RequestBooksDto requestBooksDto) {
        Books books = bookMapper.dtoToBook(requestBooksDto);
        books.setType(Type.of(requestBooksDto.getTypeId()));
        books.setAuthor(Author.of(requestBooksDto.getAuthorId()));
        books.setTagsSet(requestBooksDto.getTagsIds().stream().map(Tags::of).collect(Collectors.toSet()));
        books.setId(id);
        booksRepository.save(books);
    }

    @Override
    public void deleteBook(Long id) {
        Books books = booksRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("BOOK", "ID", id, ErrorCode.BOOK_ID_NOT_FOUND));
        booksRepository.delete(books);
    }

    @Override
    public void changeStock(Long id) {
        Books books = booksRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("BOOK", "ID", id, ErrorCode.BOOK_ID_NOT_FOUND));
        if (books.getStock() == 0) {
            throw new StockEmptyException(STOCK_UNAVAILABLE);
        } else {
            books.setStock(books.getStock() - 1);
            booksRepository.save(books);
        }
    }

}
