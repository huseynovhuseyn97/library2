package example.Library.service.impl;

import example.Library.DTO.requestDto.RequestTagsDto;
import example.Library.DTO.responseDto.ResponseTagsDto;
import example.Library.Exception.ErrorCode;
import example.Library.Exception.ResourceNotFoundException;
import example.Library.domain.Tags;
import example.Library.mapper.TagsMapper;
import example.Library.repository.TagsRepository;
import example.Library.service.TagsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TagsServiceImpl implements TagsService {

    private final TagsMapper tagsMapper;
    private final TagsRepository tagsRepository;

    @Override
    public void createTag(RequestTagsDto tagsDto) {
        tagsRepository.save(tagsMapper.dtoToTag(tagsDto));
    }

    @Override
    public List<ResponseTagsDto> findAll() {
        List<Tags> tags = tagsRepository.findAll();
        return tagsMapper.listToListDto(tags);
    }

    @Override
    public ResponseTagsDto findById(Long id) {
        Tags tags = tagsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Tags", "ID", id, ErrorCode.TAGS_ID_NOT_FOUND));
        return tagsMapper.tagToDto(tags);
    }

    @Override
    public void update(Long id, RequestTagsDto tagsDto) {
       Tags tags = tagsMapper.dtoToTag(tagsDto);
       tags.setId(id);
       tagsRepository.save(tags);
    }

    @Override
    public void deleteTag(Long id) {
        Tags tags = tagsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Tags", "ID", id, ErrorCode.TAGS_ID_NOT_FOUND));
        tagsRepository.delete(tags);
    }
}
