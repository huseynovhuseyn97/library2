package example.Library.service.impl;

import example.Library.DTO.requestDto.RequestTypeDto;
import example.Library.DTO.responseDto.ResponseTypeDto;
import example.Library.Exception.ErrorCode;
import example.Library.Exception.ResourceNotFoundException;
import example.Library.domain.Type;
import example.Library.mapper.TypeMapper;
import example.Library.repository.TypeRepository;
import example.Library.service.TypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TypeServiceImpl implements TypeService {

    public final TypeRepository typeRepository;
    public final TypeMapper typeMapper;

    @Override
    public void createType(RequestTypeDto typeDto) {
        typeRepository.save(typeMapper.dtoToType(typeDto));
    }

    @Override
    public List<ResponseTypeDto> findAll() {
        List<Type> types = typeRepository.findAll();
        if (types.isEmpty()) {
            throw new ResourceNotFoundException("Type", "ID", null, ErrorCode.TYPE_NOT_FOUND);
        }
        return typeMapper.listToListDto(types);
    }

    @Override
    public ResponseTypeDto findById(Long id) {
        Type type = typeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Type", "ID", id, ErrorCode.TYPE_ID_NOT_FOUND));
        return typeMapper.typeToDto(type);
    }

    @Override
    public void update(Long id, RequestTypeDto typeDto) {
        Type type = typeMapper.dtoToType(typeDto);
        type.setId(id);
        typeRepository.save(type);
    }

    @Override
    public void deleteType(Long id) {
        Type type = typeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Type", "ID", id, ErrorCode.TYPE_ID_NOT_FOUND));
        typeRepository.delete(type);
    }
}
