package example.Library.service.impl;

import example.Library.DTO.requestDto.RequestRentDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseRentDto;
import example.Library.Exception.ResourceNotFoundException;
import example.Library.domain.*;
import example.Library.mapper.RentMapper;
import example.Library.repository.RentRepository;
import example.Library.service.BooksService;
import example.Library.service.CustomerService;
import example.Library.service.RentService;
import example.Library.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import static example.Library.Exception.ErrorCode.RENT_ID_NOT_FOUND;

@Service
@RequiredArgsConstructor
@Transactional
public class RentServiceImpl implements RentService {


    private final SysUserService sysUserService;
    private final RentRepository rentRepository;
    private final CustomerService customerService;
    private final RentMapper rentMapper;
    private final BooksService booksService;

    @Override
    public void createRent(RequestRentDto requestRentDto) {

        SysUser rentUser = sysUserService.getUser();
        Customer customer = customerService.checkCustomer(rentUser.getId());
        Rent rent = rentMapper.dtoToRent(requestRentDto);
        Books books = Books.of(requestRentDto.getBookId());
        booksService.changeStock(requestRentDto.getBookId());
        rent.setCustomer(customer);
        rent.setStatus(RentStatus.BOOKED);
        rent.setBooks(books);
        rent.setStartDate(LocalDate.now());
        rentRepository.save(rent);
    }

    @Override
    public PageResponse<ResponseRentDto> findAll(int pageNumber, int pageSize) {
        var rentPage = rentRepository.findAll(PageRequest.of(pageNumber, pageSize));
        var pageResponse = new PageResponse<ResponseRentDto>();
        pageResponse = pageResponse
                .setPageNumber(pageNumber)
                .setPageSize(pageSize)
                .setTotalPages(rentPage.getTotalPages())
                .setTotalElements(rentPage.getTotalElements())
                .setData(rentMapper.listToListDto(rentPage.toList()));
        return pageResponse;
    }

    @Override
    public ResponseRentDto findById(Long id) {

        return rentMapper.rentToDto(rentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("RENT", "ID", id, RENT_ID_NOT_FOUND)));
    }


}
