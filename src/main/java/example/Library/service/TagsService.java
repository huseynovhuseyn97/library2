package example.Library.service;

import example.Library.DTO.requestDto.RequestTagsDto;
import example.Library.DTO.responseDto.ResponseTagsDto;

import java.util.List;

public interface TagsService {
    void createTag(RequestTagsDto tagsDto);

    List<ResponseTagsDto> findAll();

    ResponseTagsDto findById(Long id);

    void update(Long id, RequestTagsDto tagsDto);

    void deleteTag(Long id);
}
