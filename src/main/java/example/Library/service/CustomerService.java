package example.Library.service;

import example.Library.DTO.requestDto.RequestCustomerDto;
import example.Library.DTO.responseDto.PageResponse;
import example.Library.DTO.responseDto.ResponseCustomerDto;
import example.Library.domain.Customer;

public interface CustomerService {

    Customer checkCustomer (Long checkUserId);
    void createCustomer(RequestCustomerDto requestCustomerDto);

    void updateCustomerDetails(RequestCustomerDto requestCustomerDto);

    PageResponse<ResponseCustomerDto> findAll(int pageNumber, int pageSize);

    ResponseCustomerDto findCustomer(Long id);
}
