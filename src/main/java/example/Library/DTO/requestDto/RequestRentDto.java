package example.Library.DTO.requestDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestRentDto {
    private LocalDate endDate;
    private Boolean enable;
    private Long bookId;
    private int optimisticLock;
}
