package example.Library.DTO.requestDto;

import example.Library.domain.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RequestSysUserDto {
    private String username;
    private String password;
    private int optimisticLock;
}