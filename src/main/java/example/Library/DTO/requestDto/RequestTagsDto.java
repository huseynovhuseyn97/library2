package example.Library.DTO.requestDto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RequestTagsDto {
    @NotNull
    private String name;
    private Boolean enable;
    private int optimisticLock;
}
