package example.Library.DTO.requestDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RequestLoginDto {
    private String username;
    private String password;
    private int optimisticLock;
}
