package example.Library.DTO.requestDto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestTypeDto {
    @NotNull
    private String name;
    private Boolean enable;
    private int optimisticLock;
}
