package example.Library.DTO.requestDto;

import jakarta.validation.Valid;
import lombok.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Valid
public class RequestBooksDto {
    private String name;
    private Boolean enable;
    private Long typeId;
    private Long authorId;
    private Set<Long> tagsIds;
    private Integer stock;
    private int optimisticLock;

}
