package example.Library.DTO.requestDto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestAuthorDto {
    @NotNull
    private String fullName;
    private Boolean enable;
    private int optimisticLock;
}
