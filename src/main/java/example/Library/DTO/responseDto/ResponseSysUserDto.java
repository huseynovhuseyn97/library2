package example.Library.DTO.responseDto;

import example.Library.domain.Authority;
import example.Library.domain.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ResponseSysUserDto {
    private String username;
    private String password;
    List<Authority> authorities;
}
