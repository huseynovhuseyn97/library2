package example.Library.DTO.responseDto;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import static example.Library.domain.RentStatus.BOOKED;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Valid
public class ResponseRentDto {
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;
    private Boolean enable;
    private String status;
    private String customerName;
    private Long bookId;


    public  boolean getExpired() {
        if (status.equals(BOOKED) && endDate.isBefore(LocalDate.now())) {
            return true;
        } else {
            return false;
        }
    }
}
