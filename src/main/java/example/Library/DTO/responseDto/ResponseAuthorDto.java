package example.Library.DTO.responseDto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseAuthorDto {
    private Long id;
    @NotNull
    private String fullName;
    private Boolean enable;
}
