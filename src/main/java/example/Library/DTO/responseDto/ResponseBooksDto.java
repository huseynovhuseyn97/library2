package example.Library.DTO.responseDto;

import example.Library.domain.Author;
import example.Library.domain.Tags;
import example.Library.domain.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseBooksDto {
    private Long id;
    private String name;
    private Boolean enable;
    private Integer stock;
    private Type type;
    private Author author;
    private Set<Tags> tags;

}
