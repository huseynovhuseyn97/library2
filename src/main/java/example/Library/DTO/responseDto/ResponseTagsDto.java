package example.Library.DTO.responseDto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ResponseTagsDto {
    private Long id;
    @NotNull
    private String name;
    private Boolean enable;
}
