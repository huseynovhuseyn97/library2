package example.Library.repository;

import example.Library.domain.Authority;
import example.Library.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuhtorityRepository extends JpaRepository<Authority,Long> {

    Authority findByRole (Role role);
}
