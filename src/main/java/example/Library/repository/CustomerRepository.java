package example.Library.repository;

import example.Library.domain.Customer;
import example.Library.domain.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
    Optional<Customer> findBySysUser(SysUser sysUser);
}
