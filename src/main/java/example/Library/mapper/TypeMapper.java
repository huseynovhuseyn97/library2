package example.Library.mapper;

import example.Library.DTO.requestDto.RequestTypeDto;
import example.Library.DTO.responseDto.ResponseTypeDto;
import example.Library.domain.Type;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface TypeMapper {
    Type dtoToType(RequestTypeDto typeDto);

    List<ResponseTypeDto> listToListDto(List<Type> all);

    ResponseTypeDto typeToDto(Type type);

}