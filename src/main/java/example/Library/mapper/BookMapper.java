package example.Library.mapper;


import example.Library.DTO.requestDto.RequestBooksDto;
import example.Library.DTO.responseDto.ResponseBooksDto;
import example.Library.domain.Books;
import org.mapstruct.Mapper;
import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)

public interface BookMapper {
    Books dtoToBook(RequestBooksDto booksDto);

    List<ResponseBooksDto> listToListDto(List<Books> all);

    ResponseBooksDto bookToDto(Books books);

}
