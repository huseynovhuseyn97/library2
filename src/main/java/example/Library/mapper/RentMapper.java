package example.Library.mapper;

import example.Library.DTO.requestDto.RequestRentDto;
import example.Library.DTO.responseDto.ResponseRentDto;
import example.Library.domain.Rent;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring")

public interface RentMapper {
    Rent dtoToRent(RequestRentDto requestRentDto);

    List<ResponseRentDto> listToListDto(List<Rent> list);

    ResponseRentDto rentToDto(Rent rent);
}
