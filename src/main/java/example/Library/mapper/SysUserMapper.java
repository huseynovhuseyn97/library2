package example.Library.mapper;

import example.Library.DTO.responseDto.ResponseSysUserDto;
import example.Library.domain.SysUser;
import org.mapstruct.Mapper;

import java.util.List;
import static org.mapstruct.ReportingPolicy.IGNORE;


@Mapper( componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface SysUserMapper {
    List<ResponseSysUserDto> listToListDto(List<SysUser> list);

    ResponseSysUserDto userToDto(SysUser sysUser);
}
