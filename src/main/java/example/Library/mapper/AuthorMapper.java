package example.Library.mapper;

import example.Library.DTO.requestDto.RequestAuthorDto;
import example.Library.DTO.responseDto.ResponseAuthorDto;
import example.Library.domain.Author;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface AuthorMapper {

    Author dtoToAuthor(RequestAuthorDto requestAuthorDto);
    List<ResponseAuthorDto> listToListDto(List<Author> all);
    ResponseAuthorDto authorToDto(Author author);


}
