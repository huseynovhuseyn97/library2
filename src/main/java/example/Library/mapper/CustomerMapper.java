package example.Library.mapper;
import example.Library.DTO.requestDto.RequestCustomerDto;
import example.Library.DTO.responseDto.ResponseCustomerDto;
import example.Library.domain.Customer;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;
@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface CustomerMapper {

    Customer dtoToCustomer(RequestCustomerDto requestCustomerDto);

    ResponseCustomerDto customerToDto(Customer customer);

    List<ResponseCustomerDto> listToListDto(List<Customer> list);
}
