package example.Library.mapper;

import example.Library.DTO.requestDto.RequestTagsDto;
import example.Library.DTO.responseDto.ResponseTagsDto;
import example.Library.domain.Tags;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface TagsMapper {

    Tags dtoToTag(RequestTagsDto tagsDto);

    List<ResponseTagsDto> listToListDto(List<Tags> all);

    ResponseTagsDto tagToDto(Tags tags);
}
