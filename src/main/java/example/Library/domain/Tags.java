package example.Library.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tags {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Boolean enable;
    @Version
    private int optimisticLock;

    @ManyToMany(mappedBy = "tagsSet")
    private Set<Books> booksSet;

    public Tags(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tags tags = (Tags) o;
        return Objects.equals(id, tags.id) && Objects.equals(name, tags.name) && Objects.equals(enable, tags.enable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, enable);
    }

    public static Tags of(long id) {
        return new Tags(id);
    }
}
