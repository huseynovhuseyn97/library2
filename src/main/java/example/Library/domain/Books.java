package example.Library.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Books {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Integer stock;

    private Boolean enable;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    private Author author;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type type;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "books_tags",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tags> tagsSet;
    @Version
    private int optimisticLock;
    public Books(Long id) {
        this.id = id;
    }
    public static Books of(Long id) {
        return Books.builder()
                .id(id)
                .build();
    }

}
