package example.Library.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Boolean enable;
    @OneToMany(mappedBy = "type")
    private Set<Books> booksSet;
    @Version
    private int optimisticLock;

    public Type(Long id) {
        this.id = id;
    }
    public static Type of(long id){
        return new Type(id);
    }
}
