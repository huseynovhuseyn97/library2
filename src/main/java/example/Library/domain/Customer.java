package example.Library.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private LocalDate birthDate;
    private String email;
    private String phoneNumber;
    private Boolean enable;
    @Version
    private int optimisticLock;

    @OneToOne
    @JoinColumn(name = "user_id")
    private SysUser sysUser;

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    private Set<Rent> rents;
}
