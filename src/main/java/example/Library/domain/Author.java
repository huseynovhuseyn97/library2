package example.Library.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String fullName;
    private Boolean enable;
    @OneToMany(mappedBy = "author")
    private Set<Books> booksSet;
    @Version
    private int optimisticLock;

    public Author(Long id) {
        this.id = id;
    }

    public static Author of(long id) {
        return new Author(id);
    }
}
