package example.Library.domain;

public enum RentStatus {
    BOOKED,
    RENTED,
    RETURNED
    
}
