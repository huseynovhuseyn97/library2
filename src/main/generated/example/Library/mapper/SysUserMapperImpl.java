package example.Library.mapper;

import example.Library.DTO.responseDto.ResponseSysUserDto;
import example.Library.DTO.responseDto.ResponseSysUserDto.ResponseSysUserDtoBuilder;
import example.Library.domain.Authority;
import example.Library.domain.SysUser;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-14T17:12:42+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class SysUserMapperImpl implements SysUserMapper {

    @Override
    public List<ResponseSysUserDto> listToListDto(List<SysUser> list) {
        if ( list == null ) {
            return null;
        }

        List<ResponseSysUserDto> list1 = new ArrayList<ResponseSysUserDto>( list.size() );
        for ( SysUser sysUser : list ) {
            list1.add( userToDto( sysUser ) );
        }

        return list1;
    }

    @Override
    public ResponseSysUserDto userToDto(SysUser sysUser) {
        if ( sysUser == null ) {
            return null;
        }

        ResponseSysUserDtoBuilder responseSysUserDto = ResponseSysUserDto.builder();

        responseSysUserDto.username( sysUser.getUsername() );
        responseSysUserDto.password( sysUser.getPassword() );
        List<Authority> list = sysUser.getAuthorities();
        if ( list != null ) {
            responseSysUserDto.authorities( new ArrayList<Authority>( list ) );
        }

        return responseSysUserDto.build();
    }
}
