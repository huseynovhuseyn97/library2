package example.Library.mapper;

import example.Library.DTO.requestDto.RequestCustomerDto;
import example.Library.DTO.responseDto.ResponseCustomerDto;
import example.Library.DTO.responseDto.ResponseCustomerDto.ResponseCustomerDtoBuilder;
import example.Library.domain.Customer;
import example.Library.domain.Customer.CustomerBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-14T17:12:42+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class CustomerMapperImpl implements CustomerMapper {

    @Override
    public Customer dtoToCustomer(RequestCustomerDto requestCustomerDto) {
        if ( requestCustomerDto == null ) {
            return null;
        }

        CustomerBuilder customer = Customer.builder();

        customer.name( requestCustomerDto.getName() );
        customer.surname( requestCustomerDto.getSurname() );
        customer.birthDate( requestCustomerDto.getBirthDate() );
        customer.email( requestCustomerDto.getEmail() );
        customer.phoneNumber( requestCustomerDto.getPhoneNumber() );
        customer.enable( requestCustomerDto.getEnable() );
        customer.optimisticLock( requestCustomerDto.getOptimisticLock() );

        return customer.build();
    }

    @Override
    public ResponseCustomerDto customerToDto(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        ResponseCustomerDtoBuilder responseCustomerDto = ResponseCustomerDto.builder();

        responseCustomerDto.id( customer.getId() );
        responseCustomerDto.name( customer.getName() );
        responseCustomerDto.surname( customer.getSurname() );
        responseCustomerDto.birthDate( customer.getBirthDate() );
        responseCustomerDto.email( customer.getEmail() );
        responseCustomerDto.phoneNumber( customer.getPhoneNumber() );
        responseCustomerDto.enable( customer.getEnable() );

        return responseCustomerDto.build();
    }

    @Override
    public List<ResponseCustomerDto> listToListDto(List<Customer> list) {
        if ( list == null ) {
            return null;
        }

        List<ResponseCustomerDto> list1 = new ArrayList<ResponseCustomerDto>( list.size() );
        for ( Customer customer : list ) {
            list1.add( customerToDto( customer ) );
        }

        return list1;
    }
}
