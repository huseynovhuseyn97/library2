package example.Library.mapper;

import example.Library.DTO.requestDto.RequestTagsDto;
import example.Library.DTO.responseDto.ResponseTagsDto;
import example.Library.DTO.responseDto.ResponseTagsDto.ResponseTagsDtoBuilder;
import example.Library.domain.Tags;
import example.Library.domain.Tags.TagsBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-14T17:12:42+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class TagsMapperImpl implements TagsMapper {

    @Override
    public Tags dtoToTag(RequestTagsDto tagsDto) {
        if ( tagsDto == null ) {
            return null;
        }

        TagsBuilder tags = Tags.builder();

        tags.name( tagsDto.getName() );
        tags.enable( tagsDto.getEnable() );
        tags.optimisticLock( tagsDto.getOptimisticLock() );

        return tags.build();
    }

    @Override
    public List<ResponseTagsDto> listToListDto(List<Tags> all) {
        if ( all == null ) {
            return null;
        }

        List<ResponseTagsDto> list = new ArrayList<ResponseTagsDto>( all.size() );
        for ( Tags tags : all ) {
            list.add( tagToDto( tags ) );
        }

        return list;
    }

    @Override
    public ResponseTagsDto tagToDto(Tags tags) {
        if ( tags == null ) {
            return null;
        }

        ResponseTagsDtoBuilder responseTagsDto = ResponseTagsDto.builder();

        responseTagsDto.id( tags.getId() );
        responseTagsDto.name( tags.getName() );
        responseTagsDto.enable( tags.getEnable() );

        return responseTagsDto.build();
    }
}
