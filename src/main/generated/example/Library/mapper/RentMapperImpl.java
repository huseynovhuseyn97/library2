package example.Library.mapper;

import example.Library.DTO.requestDto.RequestRentDto;
import example.Library.DTO.responseDto.ResponseRentDto;
import example.Library.DTO.responseDto.ResponseRentDto.ResponseRentDtoBuilder;
import example.Library.domain.Rent;
import example.Library.domain.Rent.RentBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-14T17:12:42+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class RentMapperImpl implements RentMapper {

    @Override
    public Rent dtoToRent(RequestRentDto requestRentDto) {
        if ( requestRentDto == null ) {
            return null;
        }

        RentBuilder rent = Rent.builder();

        rent.endDate( requestRentDto.getEndDate() );
        rent.enable( requestRentDto.getEnable() );
        rent.optimisticLock( requestRentDto.getOptimisticLock() );

        return rent.build();
    }

    @Override
    public List<ResponseRentDto> listToListDto(List<Rent> list) {
        if ( list == null ) {
            return null;
        }

        List<ResponseRentDto> list1 = new ArrayList<ResponseRentDto>( list.size() );
        for ( Rent rent : list ) {
            list1.add( rentToDto( rent ) );
        }

        return list1;
    }

    @Override
    public ResponseRentDto rentToDto(Rent rent) {
        if ( rent == null ) {
            return null;
        }

        ResponseRentDtoBuilder responseRentDto = ResponseRentDto.builder();

        responseRentDto.id( rent.getId() );
        responseRentDto.startDate( rent.getStartDate() );
        responseRentDto.endDate( rent.getEndDate() );
        responseRentDto.enable( rent.getEnable() );
        if ( rent.getStatus() != null ) {
            responseRentDto.status( rent.getStatus().name() );
        }

        return responseRentDto.build();
    }
}
