package example.Library.mapper;

import example.Library.DTO.requestDto.RequestAuthorDto;
import example.Library.DTO.responseDto.ResponseAuthorDto;
import example.Library.DTO.responseDto.ResponseAuthorDto.ResponseAuthorDtoBuilder;
import example.Library.domain.Author;
import example.Library.domain.Author.AuthorBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-13T18:20:46+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class AuthorMapperImpl implements AuthorMapper {

    @Override
    public Author dtoToAuthor(RequestAuthorDto requestAuthorDto) {
        if ( requestAuthorDto == null ) {
            return null;
        }

        AuthorBuilder author = Author.builder();

        author.fullName( requestAuthorDto.getFullName() );
        author.enable( requestAuthorDto.getEnable() );
        author.optimisticLock( requestAuthorDto.getOptimisticLock() );

        return author.build();
    }

    @Override
    public List<ResponseAuthorDto> listToListDto(List<Author> all) {
        if ( all == null ) {
            return null;
        }

        List<ResponseAuthorDto> list = new ArrayList<ResponseAuthorDto>( all.size() );
        for ( Author author : all ) {
            list.add( authorToDto( author ) );
        }

        return list;
    }

    @Override
    public ResponseAuthorDto authorToDto(Author author) {
        if ( author == null ) {
            return null;
        }

        ResponseAuthorDtoBuilder responseAuthorDto = ResponseAuthorDto.builder();

        responseAuthorDto.id( author.getId() );
        responseAuthorDto.fullName( author.getFullName() );
        responseAuthorDto.enable( author.getEnable() );

        return responseAuthorDto.build();
    }
}
