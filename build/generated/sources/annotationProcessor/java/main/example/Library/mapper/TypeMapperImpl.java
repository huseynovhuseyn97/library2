package example.Library.mapper;

import example.Library.DTO.requestDto.RequestTypeDto;
import example.Library.DTO.responseDto.ResponseTypeDto;
import example.Library.DTO.responseDto.ResponseTypeDto.ResponseTypeDtoBuilder;
import example.Library.domain.Type;
import example.Library.domain.Type.TypeBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-13T18:20:46+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class TypeMapperImpl implements TypeMapper {

    @Override
    public Type dtoToType(RequestTypeDto typeDto) {
        if ( typeDto == null ) {
            return null;
        }

        TypeBuilder type = Type.builder();

        type.name( typeDto.getName() );
        type.enable( typeDto.getEnable() );
        type.optimisticLock( typeDto.getOptimisticLock() );

        return type.build();
    }

    @Override
    public List<ResponseTypeDto> listToListDto(List<Type> all) {
        if ( all == null ) {
            return null;
        }

        List<ResponseTypeDto> list = new ArrayList<ResponseTypeDto>( all.size() );
        for ( Type type : all ) {
            list.add( typeToDto( type ) );
        }

        return list;
    }

    @Override
    public ResponseTypeDto typeToDto(Type type) {
        if ( type == null ) {
            return null;
        }

        ResponseTypeDtoBuilder responseTypeDto = ResponseTypeDto.builder();

        responseTypeDto.id( type.getId() );
        responseTypeDto.name( type.getName() );
        responseTypeDto.enable( type.getEnable() );

        return responseTypeDto.build();
    }
}
