package example.Library.mapper;

import example.Library.DTO.requestDto.RequestBooksDto;
import example.Library.DTO.responseDto.ResponseBooksDto;
import example.Library.DTO.responseDto.ResponseBooksDto.ResponseBooksDtoBuilder;
import example.Library.domain.Books;
import example.Library.domain.Books.BooksBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-13T18:20:46+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class BookMapperImpl implements BookMapper {

    @Override
    public Books dtoToBook(RequestBooksDto booksDto) {
        if ( booksDto == null ) {
            return null;
        }

        BooksBuilder books = Books.builder();

        books.name( booksDto.getName() );
        books.stock( booksDto.getStock() );
        books.enable( booksDto.getEnable() );
        books.optimisticLock( booksDto.getOptimisticLock() );

        return books.build();
    }

    @Override
    public List<ResponseBooksDto> listToListDto(List<Books> all) {
        if ( all == null ) {
            return null;
        }

        List<ResponseBooksDto> list = new ArrayList<ResponseBooksDto>( all.size() );
        for ( Books books : all ) {
            list.add( bookToDto( books ) );
        }

        return list;
    }

    @Override
    public ResponseBooksDto bookToDto(Books books) {
        if ( books == null ) {
            return null;
        }

        ResponseBooksDtoBuilder responseBooksDto = ResponseBooksDto.builder();

        responseBooksDto.id( books.getId() );
        responseBooksDto.name( books.getName() );
        responseBooksDto.enable( books.getEnable() );
        responseBooksDto.stock( books.getStock() );
        responseBooksDto.type( books.getType() );
        responseBooksDto.author( books.getAuthor() );

        return responseBooksDto.build();
    }
}
