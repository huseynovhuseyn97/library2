package example.Library.mapper;

import example.Library.DTO.responseDto.ResponseCustomerDto;
import example.Library.domain.Customer;
import example.Library.domain.Customer.CustomerBuilder;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-13T18:20:46+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class CustomerMapperImpl implements CustomerMapper {

    @Override
    public Customer dtoToCustomer(ResponseCustomerDto responseCustomerDto) {
        if ( responseCustomerDto == null ) {
            return null;
        }

        CustomerBuilder customer = Customer.builder();

        customer.id( responseCustomerDto.getId() );
        customer.name( responseCustomerDto.getName() );
        customer.surname( responseCustomerDto.getSurname() );
        customer.birthDate( responseCustomerDto.getBirthDate() );
        customer.email( responseCustomerDto.getEmail() );
        customer.phoneNumber( responseCustomerDto.getPhoneNumber() );
        customer.enable( responseCustomerDto.getEnable() );

        return customer.build();
    }
}
